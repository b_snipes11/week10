#Project 9
#11/10/15
#Brady Russ

codes = {'A':'1', 'B':'2', 'C':'3', 'D':'4', 'E':'5', 'F':'6', 'G':'7', 'H':'8', 'I':'9', 'J':'0', 'K':'p', 'L':'o', 'M':'i', 'N':'u', 'O':'y', 'P':'t',
          'Q':'r', 'R':'e', 'S':'w', 'T':'q', 'U':'a', 'V':'s', 'W':'d', 'X':'f', 'Y':'g', 'Z':'h', 'a':'j', 'b':'k', 'c':'l', 'd':'m', 'e':'n', 'f':'b',
          'g':'v', 'h':'c', 'i':'x', 'j':'z', 'k':'Z', 'l':'X', 'm':'C', 'n':'V', 'o':'B', 'p':'N', 'q':'M', 'r':'L', 's':'K', 't':'J', 'u':'H', 'v':'G',
          'w':'F', 'x':'D', 'y':'S', 'z':'A', '(':'Q', ')':'W', ',':'E', '.':'R', '?':'T', '[':'Y', ']':'U', '\'':'I', '1':'O', '2':'P', ' ':'!', '\n':'+'}

bigdustyFile = open("Bigdusty.txt", 'r')

txt = bigdustyFile.read()
bigdustyFile.close()

encryptString = ""

txtChar = list(txt)

for x in txtChar:
    char = codes[x]
    encryptString += char

print(encryptString)

output = open("encrypted.txt", 'w')
output.write(encryptString)
output.close()

