#Project 9 Part II
#11/16/15
#Brady Russ

codes = {'A':'1', 'B':'2', 'C':'3', 'D':'4', 'E':'5', 'F':'6', 'G':'7', 'H':'8', 'I':'9', 'J':'0', 'K':'p', 'L':'o', 'M':'i', 'N':'u', 'O':'y', 'P':'t',
          'Q':'r', 'R':'e', 'S':'w', 'T':'q', 'U':'a', 'V':'s', 'W':'d', 'X':'f', 'Y':'g', 'Z':'h', 'a':'j', 'b':'k', 'c':'l', 'd':'m', 'e':'n', 'f':'b',
          'g':'v', 'h':'c', 'i':'x', 'j':'z', 'k':'Z', 'l':'X', 'm':'C', 'n':'V', 'o':'B', 'p':'N', 'q':'M', 'r':'L', 's':'K', 't':'J', 'u':'H', 'v':'G',
          'w':'F', 'x':'D', 'y':'S', 'z':'A', '(':'Q', ')':'W', ',':'E', '.':'R', '?':'T', '[':'Y', ']':'U', '\'':'I', '1':'O', '2':'P', ' ':'!', '\n':'+', '=':'@'}

decode = dict(zip(codes.values(), codes))

encryptedFile = open("Encrypted.txt", 'r')

txt= encryptedFile.read()
encryptedFile.close()

encryptedString = ""

txtChar = list(txt)

for x in txtChar:
    char = decode[x]
    encryptedString += char

print(encryptedString)

output = open("encrypted.txt", 'w')
output.write(encryptedString)
output.close()
